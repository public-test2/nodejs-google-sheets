# NODEJS-GOOGLE SHEETS

Prueba para leer/insertar datos en Google Sheets con NodeJS

- [Instalar dependencias](#instalar-dependencias)
- [Preparar variables de entorno](#preparar-variables-de-entorno)
  - [Ejemplo de .env](#ejemplo-de-env)
- [Credenciales de acceso](#credenciales-de-acceso)
  - [Ejemplo de credentials.json](#ejemplo-de-credentialsjson)
- [Correr proyecto](#correr-proyecto)
- [Obtener credenciales con Google Cloud](#obtener-credenciales-con-google-cloud)
- [Compartir documento de sheets](#compartir-documento-de-sheets)

## Instalar dependencias 

`npm install` ó `npm i`

## Preparar variables de entorno

- Crear un archivo con el nombre `.env` a la par con **package.json** y agregar las siguiente variables
  - **HTTP_PORT** : _Int_ - Puerto donde correrá el proyecto
  - **GOOGLE_SHEET_ID** : _String_ Id del documento*

```
project/
| img/
| node_modules/
| src/ 
| .babelrc
| .env
| package.json
```

_* El id del documento correponde a esta serie de caractéres *_
 
<img src="img/document_id.png" />

#### Ejemplo de .env

```.env
HTTP_PORT=3000
GOOGLE_SHEET_ID=1J8nG6X7HvHeXrnN0PQHh90Tmp_43yBUgY_xaT3NnFCI
```

## Credenciales de acceso

```
project/
| ...
| src/
| | keys/
| | |  credentials.json
```

#### Ejemplo de credentials.json

```json
{
  "type": "service_account",
  "project_id": "icon...",
  "private_key_id": "5177953345a1a0a8ae...",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgk...",
  "client_email": "googlesheet-node@ic...",
  "client_id": "10119147148...",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/meta..."
}
```

## Correr proyecto

`npm run dev`

## Obtener credenciales con Google Cloud

<div style="text-align:center;"> 
    <img src="img/t-01.png" />
</div>

<div style="text-align:center;"> 
    <img src="img/t-02.png" />
</div>

#### Habilitar Google Drive API
<div style="text-align:center;"> 
    <img src="img/t-03.png" />
</div>

<div style="text-align:center;"> 
    <img src="img/t-04.png" />
</div>

#### Crear credenciales
<div style="text-align:center;"> 
    <img src="img/t-05.png" />
</div>

<div style="text-align:center;"> 
    <img src="img/t-06.png" />
</div>

<div style="text-align:center;"> 
    <img src="img/t-07.png" />
</div>

<div style="text-align:center;"> 
    <img src="img/t-08.png" />
</div>

<div style="text-align:center;"> 
    <img src="img/t-09.png" />
</div>

<div style="text-align:center;margin-bottom:30px"> 
    <img src="img/t-10.png" />
</div>

#### Descargar credenciales
<div style="text-align:center;margin:0 0 55px 0"> 
    <img src="img/t-11.png" />
</div>

<div style="text-align:center;"> 
    <img src="img/t-12.png" />
</div>

<div style="text-align:center;margin:0 0 25px 0"> 
    <img src="img/t-13.png" />
</div>

Este archivo corresponde a **credentials.json** por lo que deberá ser renombrado
<div style="text-align:center;margin:0 0 25px 0"> 
    <img src="img/t-14.png" />
</div>

#### Habilitar Google Sheets API
<div style="text-align:center;"> 
    <img src="img/t-15.png" />
</div>

<div style="text-align:center;"> 
    <img src="img/t-16.png" />
</div>

## Compartir documento de sheets

De nuestro archivo **credentials.json** tomaremos el valor de la propiedad **client_email** y es el correo que utilizaremos para compartir el documento de sheets.
<div style="text-align:center;"> 
    <img src="img/t-17.png" />
</div>

