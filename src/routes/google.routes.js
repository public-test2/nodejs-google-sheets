import { Router } from 'express';
import Controller from '../controllers/google.controller';

const router = Router();

// GET
router.get('/', Controller.getIndexView);
router.get('/form', Controller.getFormView);
router.get('/create-folder', Controller.getCreateFolderView);
router.get('/create-sheet', Controller.getCreateSheetView);
router.get('/create-sheet-folder', Controller.getCreateSheetFolderView);
router.get('/create-sheet-folder-data', Controller.getCreateSheetFolderDataView);
// POST
router.post('/form', Controller.saveData);
router.post('/create-folder', Controller.CreateFolder);
router.post('/create-sheet', Controller.createSheetWithFolderId);
router.post('/create-sheet-folder', Controller.createSheetAndFolder);
router.post('/create-sheet-folder-data', Controller.createSheetAndFolderWithData);

export default router;