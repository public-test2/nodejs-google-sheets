import dotenv from 'dotenv/config';
import app from './app';

const PORT = process.env.HTTP_PORT;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`))