document.querySelector("#myForm").addEventListener("submit", function(e){
    try {
        const sheet = document.getElementById("sheet-input").value;
        const folder = document.getElementById("folder-input").value;
        const email = document.getElementById("email-input").value;

        if(!validateFields(sheet, folder, email)) {
            validateErrors(sheet, folder, email)
            e.preventDefault()
        }
    
    } catch (err) {
        console.error(err);
        e.preventDefault();
    }
});

const validateFields = (sheet, folder, email) => {
    if(sheet.trim().length > 0 && folder.trim().length > 0 && email.trim().length > 0) return true;
    return false;
}

const validateErrors = (sheet, folder, email) => {
    if(sheet.trim().length === 0) {
        createError("¡No has ingresado el nombre!");
    }

    if(email.trim().length === 0) {
        createError("¡No has ingresado el email!");
    }

    if(folder.trim().length === 0) {
        createError("¡No has ingresado el id del folder!");
    }
}

const createError = (text) => {

    const errorContainer = document.getElementById("errors");
    let advice = document.createElement('div');
    let className = "alert" + Math.random();
    advice.className = "alert alert-danger " + className
    advice.innerText = text;
    errorContainer.appendChild(advice);

    setTimeout(() => {
        var elements = document.getElementsByClassName(className);
        while(elements.length > 0){
            elements[0].parentNode.removeChild(elements[0]);
        }
    }, 2000);

}