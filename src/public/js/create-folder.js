document.querySelector("#myForm").addEventListener("submit", function(e){
    try {
        const folderId = document.getElementById("folder-input").value;
        const email = document.getElementById("email-input").value;

        if(!validateFields(folderId, email)) {
            validateErrors(folderId, email)
            e.preventDefault()
        }
    
    } catch (err) {
        console.error(err);
        e.preventDefault();
    }
});

const validateFields = (folderId, email) => {
    if(folderId.trim().length > 0 && email.trim().length > 0) return true;
    return false;
}

const validateErrors = (folderId, email) => {

    if(email.trim().length === 0) {
        createError("¡No has ingresado el email!");
    }

    if(folderId.trim().length === 0) {
        createError("¡No has ingresado el id del folder!");
    }

}

const createError = (text) => {

    const errorContainer = document.getElementById("errors");
    let advice = document.createElement('div');
    let className = "alert" + Math.random();
    advice.className = "alert alert-danger " + className
    advice.innerText = text;
    errorContainer.appendChild(advice);

    setTimeout(() => {
        var elements = document.getElementsByClassName(className);
        while(elements.length > 0){
            elements[0].parentNode.removeChild(elements[0]);
        }
    }, 2000);

}