import express from 'express';
import routes from './routes/google.routes';
import path from 'path';

const app = express();

// Middlewares
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Routes
app.use(routes);

// Views
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug')

// Public
app.use(express.static(path.join(__dirname, './public')));

export default app;