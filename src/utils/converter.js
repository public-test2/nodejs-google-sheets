import xmlconvert from 'xml-js';
import fs from 'fs';
import path from 'path';
import jsonconvert from 'json-2-csv';

export const xml2csv = async(dir) => {
    // Obtenemos el archivo xml
    const xmlFile = fs.readFileSync(path.join(__dirname, dir));

    // xml a formato json
    let jsonConverted = xmlconvert.xml2json(xmlFile, {compact: true, spaces: 4});

    // json.parse para convertirlo en un objeto
    jsonConverted = JSON.parse(jsonConverted)

    // objeto a csv
    const csv = await jsonconvert.json2csvAsync([jsonConverted])

    // Guardar el archivo csv localmente
    fs.writeFileSync(path.join(__dirname, '../resources/data.csv'), csv, 'utf-8')
    
    return csv;
}