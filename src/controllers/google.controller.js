import { GoogleSpreadsheet } from 'google-spreadsheet';
import { google } from 'googleapis';
import { xml2csv } from '../utils/converter';

const sheets = google.sheets('v4');

const credentials = require('../keys/credentials.json');

const auth = new google.auth.JWT(
    credentials.client_email,
    null,
    credentials.private_key,
    ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive'],
    null
);

let googleId = process.env.GOOGLE_SHEET_ID;

const loadDocument = async () => {
    const document = new GoogleSpreadsheet(googleId);
    await document.useServiceAccountAuth(credentials);
    await document.loadInfo();
    return document;
}

const errorResponse = (e, res, next) => {
    console.log(e);
    res.status(500).send({
        message: e.message
    });
    next(e);
}

export default {
    getIndexView: async (req, res, next) => {
        try {
            console.log(credentials.client_email);
            // Cargar documento
            const document = await loadDocument();
            // Cargar hoja y obtener los datos
            const sheet = document.sheetsByIndex[0];
            const data = await sheet.getRows();

            res.render('index', {data});
        } catch (e) {
            errorResponse(e, res, next)
        }
    },
    getFormView: async(req, res, next) => {
        try {
            res.render('form', {});
        } catch (e) {
            errorResponse(e, res, next)
        }
    },
    getCreateSheetView: async(req, res, next) => {
        try {
            res.render('create-sheet');
        } catch (e) {
            errorResponse(e, res, next);
        }
    },
    getCreateFolderView: async(req, res, next) => {

        try {
            res.render('create-folder');

        } catch (e) {
            errorResponse(e, res, next);
        }

    },
    getCreateSheetFolderView: async(req, res, next) => {
        try {
            res.render('create-sheet-folder');
        } catch (e) {
            errorResponse(e, res, next);
        }
    },
    getCreateSheetFolderDataView: async(req, res, next) => {
        try {
            res.render('create-sheet-folder-data');
        } catch (e) {
            errorResponse(e, res, next);
        }
    },
    CreateFolder: async(req, res, next) => {

        try {
            const { foldername, email } = req.body;

            const drive = google.drive({ version: "v3", auth: auth });

            var fileMetadata = {
                'name': foldername,
                'mimeType': 'application/vnd.google-apps.folder'
              };

            const folderData = await drive.files.create({
                resource: fileMetadata,
                fields: 'id',
                transferOwnership: true,
                //moveToNewOwnersRoot: true
            });
  
            const folderId = folderData.data.id;

            // Compartir el folder
            await drive.permissions.create({
                resource: {
                    type: "user",
                    role: "owner",
                    emailAddress: email,
                },
                fileId: folderId,
                fields: "id",
                transferOwnership: true,
                moveToNewOwnersRoot: true
            });

            res.render('folder-data', {
                projectId: credentials.project_id,
                email: credentials.client_email,
                shared: email,
                folderId,
                folderName: foldername,
                folderURL : `https://drive.google.com/drive/folders/${folderId}?usp=sharing`,
            });

        } catch (e) {
            errorResponse(e, res, next);
        }

    },
    saveData: async(req, res, next) => {
        try {
            // Desestructurar datos del formulario
            const { name, lastname, age } = req.body;
            const data = {
                Name: name,
                LastName: lastname,
                Age: age
            }

            // Cargar documento
            const document = await loadDocument();
            // Cargar hoja e insertar datos
            const sheet = document.sheetsByIndex[0];
            await sheet.addRow(data);
            // Reedirección al inicio
            res.redirect('/');
        } catch (e) {
            errorResponse(e, res, next)
        }
    },
    createSheetWithFolderId: async(req, res, next) => {
        try {
            const { sheetname, folderId, email } = req.body;

            const createOption = {
                auth: auth,
                resource: {
                    properties: {
                        title: sheetname
                    }
                }
            }

            // Crear el sheet
            const response = await sheets.spreadsheets.create(createOption)

            const fileId = response.data.spreadsheetId;
            const drive = google.drive({ version: "v3", auth: auth });

            // Compartir el sheet
            await drive.permissions.create({
                resource: {
                    type: "user",
                    role: "owner",
                    emailAddress: email,
                },
                fileId: fileId,
                fields: "id",
                transferOwnership: true,
                //moveToNewOwnersRoot: true // Con este parámetro guardarmos el sheet en My Drive
            });

            await drive.files.get({
                fileId: fileId,
                fields: '*'
            }, function(err, file) {
                if (err) {
                    console.log(err);
                } else {
                    var previousParents = file.data.parents.join(',');
                    // Mover el Sheet al folder
                    drive.files.update({
                        fileId: fileId,
                        addParents: folderId,
                        removeParents: previousParents,
                        fields: 'id, parents'
                    }, function(err, file) {
                        if(err) {
                        console.log(err)
                        } else {
                            console.log("Operación exitosa")
                        }
                    });
                }
            });

            res.render('sheet-data', {
                projectId: credentials.project_id,
                email: credentials.client_email,
                shared: email,
                folderId,
                folderURL : `https://drive.google.com/drive/folders/${folderId}?usp=sharing`,
                sheetName: sheetname,
                sheetUrl: `https://docs.google.com/spreadsheets/d/${fileId}/edit`
            });

        } catch (e) {
            errorResponse(e, res, next);
        }
    },
    createSheetAndFolder: async(req, res, next) => {
        try {
            const { sheetname, foldername, email } = req.body;

            const createOption = {
                auth: auth,
                resource: {
                    properties: {
                        title: sheetname
                    }
                }
            }

            // Crear el sheet
            const response = await sheets.spreadsheets.create(createOption)

            const fileId = response.data.spreadsheetId;
            const drive = google.drive({ version: "v3", auth: auth });

            // Compartir el sheet
            await drive.permissions.create({
                resource: {
                    type: "user",
                    role: "owner",
                    emailAddress: email,
                },
                fileId: fileId,
                fields: "id",
                transferOwnership: true,
                //moveToNewOwnersRoot: true
            });

            // Crear folder
            var fileMetadata = {
                'name': foldername,
                'mimeType': 'application/vnd.google-apps.folder'
            };
            const folder = await drive.files.create({
                resource: fileMetadata,
                fields: 'id',
                transferOwnership: true,
                //moveToNewOwnersRoot: true
            });

            const folderId = folder.data.id;

            // Compartir el folder
            await drive.permissions.create({
                resource: {
                    type: "user",
                    role: "organizer", // owner
                    emailAddress: email,
                },
                fileId: folderId,
                fields: "id",
                transferOwnership: true,
                moveToNewOwnersRoot: true
            });
            
            await drive.files.get({
                fileId: fileId,
                fields: '*'
            }, function(err, file) {
                if (err) {
                    console.log(err);
                } else {
                    var previousParents = file.data.parents.join(',');
                    // Mover el sheet al folder
                    drive.files.update({
                        fileId: fileId,
                        addParents: folderId,
                        removeParents: previousParents,
                        fields: 'id, parents'
                    }, function(err, file) {
                        if(err) {
                        console.log(err)
                        } else {
                            console.log("Operación exitosa")
                        }
                    });
                }
            });
            
            res.render('sheet-data', {
                projectId: credentials.project_id,
                email: credentials.client_email,
                shared: email,
                folderId,
                folderName: foldername,
                folderURL : `https://drive.google.com/drive/folders/${folderId}?usp=sharing`,
                sheetName: sheetname,
                sheetUrl: `https://docs.google.com/spreadsheets/d/${fileId}/edit`
            });

        } catch (e) {
            errorResponse(e, res, next);
        }
    },
    createSheetAndFolderWithData: async(req, res, next) => {
        try {
            const { sheetname, foldername, email } = req.body;

            // Generar csv a partir de xml
            const csv = await xml2csv('../resources/data.xml');

            const data = csv.split("\n")
            // Extraer campos y datos
            const fields = data[0].split(",")
            const values = data[1].split(",")

            const createOption = {
                auth: auth,
                resource: {
                    properties: {
                        title: sheetname
                    }
                }
            }

            const appendOption = {
                auth: auth,
                spreadsheetId: "",
                range: "A:A",
                valueInputOption: "USER_ENTERED",
                resource: {
                    values: [ // Arreglo de arreglos
                        fields,
                        values
                    ]
                }
            }

            // Crear el sheet
            const response = await sheets.spreadsheets.create(createOption);

            const fileId = response.data.spreadsheetId;
            const drive = google.drive({ version: "v3", auth: auth });

            appendOption.spreadsheetId = fileId;
            // Introducir datos al sheet
            await sheets.spreadsheets.values.append(appendOption, function (err, responses) {
                if (err) {
                    console.error(err);
                    return;
                }
            });

            // Compartir el sheet
            await drive.permissions.create({
                resource: {
                    type: "user",
                    role: "owner",
                    emailAddress: email,
                },
                fileId: fileId,
                fields: "id",
                transferOwnership: true,
                //moveToNewOwnersRoot: true
            });

            // Crear folder
            var fileMetadata = {
                'name': foldername,
                'mimeType': 'application/vnd.google-apps.folder'
              };
            const folder = await drive.files.create({
                resource: fileMetadata,
                fields: 'id',
                transferOwnership: true,
                //moveToNewOwnersRoot: true
            });

            const folderId = folder.data.id;

            await drive.permissions.create({
                resource: {
                    type: "user",
                    role: "owner",
                    emailAddress: email,
                },
                fileId: folderId,
                fields: "id",
                transferOwnership: true,
                moveToNewOwnersRoot: true
            });

            
            await drive.files.get({
                fileId: fileId,
                fields: '*'
            }, function(err, file) {
                if (err) {
                    console.log(err);
                } else {
                    var previousParents = file.data.parents.join(',');
                    // Mover el sheet al folder
                    drive.files.update({
                        fileId: fileId,
                        addParents: folderId,
                        removeParents: previousParents,
                        fields: 'id, parents'
                    }, function(err, file) {
                        if(err) {
                            console.log(err)
                        } else {
                            console.log("Operación exitosa")
                        }
                    });
                }
            });
            
            res.render('sheet-data', {
                projectId: credentials.project_id,
                email: credentials.client_email,
                shared: email,
                folderId,
                folderName: foldername,
                folderURL : `https://drive.google.com/drive/folders/${folderId}?usp=sharing`,
                sheetName: sheetname,
                sheetUrl: `https://docs.google.com/spreadsheets/d/${fileId}/edit`
            });
        } catch (e) {
            errorResponse(e, res, next)
        }
    },
}